// -----------------------------------------------------
// RSP microcode
// Initialization of RSP
// -----------------------------------------------------

System_ByteAlign(8)
Microcode_Init:
arch n64.rsp
base $0000

// -----------------------------------------------------
Microcode_Init_Start:

    // Zero vector
    vsub v31,v31[e0]

    break

// -----------------------------------------------------
System_ByteAlign(8)
base Microcode_Init+pc()
Microcode_Init_End: